module bitbucket.org/neiku/winornot

go 1.16

require (
	github.com/gonutz/w32 v1.0.0
	github.com/rodolfoag/gow32 v0.0.0-20160917004320-d95ff468acf8
	golang.org/x/sys v0.0.0-20220908164124-27713097b956
)
