package winornot

import "github.com/rodolfoag/gow32"

// CheckInstanceExist indicates if one instance of the application is open.
func CheckInstanceExist(name string) bool {
	_, err := gow32.CreateMutex(name)
	return err != nil
}
