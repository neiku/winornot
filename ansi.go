// +build !windows

package winornot

// EnableANSIControl lets Windows cmd.exe's and PowerShell's console support ANSI escape sequences.
// Control character sequences that control cursor movement, color/font mode, and other operations can be performed.
func EnableANSIControl() {
	// Not implemented on non-Windows
}
