package winornot

import (
	"os"

	"github.com/gonutz/w32"
	"golang.org/x/sys/windows"
)

// HideConsole hides the console window.
func HideConsole() {
	displayConsoleAsync(w32.SW_HIDE)
}

// ShowConsole shows the console window.
func ShowConsole() {
	displayConsoleAsync(w32.SW_SHOW)
}

func displayConsoleAsync(commandShow int) {
	console := w32.GetConsoleWindow()
	if console != 0 {
		_, consoleProcID := w32.GetWindowThreadProcessId(console)
		if w32.GetCurrentProcessId() == consoleProcID {
			w32.ShowWindowAsync(console, commandShow)
		}
	}
}

// DisableConsoleQuickEditMode disables the user to use the mouse to select
// and edit text in the console window.
func DisableConsoleQuickEditMode() {
	stdin := windows.Handle(os.Stdin.Fd())
	var originalMode uint32

	windows.GetConsoleMode(stdin, &originalMode)
	windows.SetConsoleMode(stdin, originalMode&^windows.ENABLE_QUICK_EDIT_MODE)
}

// DisableConsoleInsertMode will overwrite the text after the current cursor
// location when text inputs are entered in the console window.
func DisableConsoleInsertMode() {
	stdin := windows.Handle(os.Stdin.Fd())
	var originalMode uint32

	windows.GetConsoleMode(stdin, &originalMode)
	windows.SetConsoleMode(stdin, originalMode&^windows.ENABLE_INSERT_MODE)
}

// DisableConsoleMouseInput disables the capute of mouse events in the console window.
func DisableConsoleMouseInput() {
	stdin := windows.Handle(os.Stdin.Fd())
	var originalMode uint32

	windows.GetConsoleMode(stdin, &originalMode)
	windows.SetConsoleMode(stdin, originalMode&^windows.ENABLE_MOUSE_INPUT)
}

// DisableConsoleExtendedEditMode disables mouse input from console window by disabling
// QUICK_EDIT_MODE and MOUSE_INPUT, it also disables the INSERT_MODE
func DisableConsoleExtendedEditMode() {
	stdin := windows.Handle(os.Stdin.Fd())
	var originalMode uint32

	windows.GetConsoleMode(stdin, &originalMode)
	originalMode &^= windows.ENABLE_QUICK_EDIT_MODE
	originalMode &^= windows.ENABLE_INSERT_MODE
	originalMode &^= windows.ENABLE_MOUSE_INPUT
	windows.SetConsoleMode(stdin, originalMode)
}
