package winornot

import (
	"os"
	"sync"

	"golang.org/x/sys/windows"
)

var enableANSI sync.Once

// EnableANSIControl lets Windows cmd.exe's and PowerShell's console support ANSI escape sequences.
// Control character sequences that control cursor movement, color/font mode, and other operations can be performed.
func EnableANSIControl() {
	enableANSI.Do(func() {
		stdout := windows.Handle(os.Stdout.Fd())
		var originalMode uint32

		windows.GetConsoleMode(stdout, &originalMode)
		windows.SetConsoleMode(stdout, originalMode|windows.ENABLE_VIRTUAL_TERMINAL_PROCESSING)
	})
}
