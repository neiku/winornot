package winornot_test

import (
	"testing"

	"bitbucket.org/neiku/winornot"
)

const logo = `
  ██╗    ██╗██╗███╗   ██╗ ██████╗ ██████╗ ███╗   ██╗ ██████╗ ████████╗
  ██║    ██║██║████╗  ██║██╔═══██╗██╔══██╗████╗  ██║██╔═══██╗╚══██╔══╝
  ██║ █╗ ██║██║██╔██╗ ██║██║   ██║██████╔╝██╔██╗ ██║██║   ██║   ██║
  ██║███╗██║██║██║╚██╗██║██║   ██║██╔══██╗██║╚██╗██║██║   ██║   ██║
  ╚███╔███╔╝██║██║ ╚████║╚██████╔╝██║  ██║██║ ╚████║╚██████╔╝   ██║
   ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝    ╚═╝
`

func TestAllWindows(t *testing.T) {
	winornot.CheckInstanceExist("test")
	winornot.EnableANSIControl()
	winornot.DisableConsoleExtendedEditMode()
	t.Log(logo)
}
