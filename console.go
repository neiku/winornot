// +build !windows

package winornot

// HideConsole hides the console window.
func HideConsole() {
	// Not implemented on non-Windows
}

// ShowConsole shows the console window.
func ShowConsole() {
	// Not implemented on non-Windows
}

// DisableConsoleQuickEditMode disables the user to use the mouse to select
// and edit text in the console window.
func DisableConsoleQuickEditMode() {
	// Not implemented on non-Windows
}

// DisableConsoleInsertMode will overwrite the text after the current cursor
// location when text inputs are entered in the console window.
func DisableConsoleInsertMode() {
	// Not implemented on non-Windows
}

// DisableConsoleMouseInput disables the capute of mouse events in the console window.
func DisableConsoleMouseInput() {
	// Not implemented on non-Windows
}

// DisableConsoleExtendedEditMode disables mouse input from console window by disabling
// QUICK_EDIT_MODE and MOUSE_INPUT, it also disables the INSERT_MODE
func DisableConsoleExtendedEditMode() {
	// Not implemented on non-Windows
}
